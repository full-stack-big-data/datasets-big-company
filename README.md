### _Set of dataset from a lot of resource, challenges and competition about Machine Learning to research all over the world_


Video: https://research.google.com/youtube8m/download.html

Registry of Open Data on AWS: https://registry.opendata.aws/

Datasets | Kaggle: https://www.kaggle.com/datasets

Stanford Large Network Dataset Collection: https://snap.stanford.edu/data/

UCI Machine Learning Repository: Data Sets: https://archive.ics.uci.edu/ml/index.php

Tianchi Data Lab: https://tianchi.aliyun.com/datalab/index.htm?spm=5176.100170.1234.4.JIi3jt

U.S. Government’s open data: https://catalog.data.gov/dataset

[data.gov.uk](https://data.gov.uk/) | Find open data

ACM RecSys Challenge 2017: http://www.recsyschallenge.com/2018/

Huge public dataset of internet scanning: https://censys.io/ 

Goverment India's data: https://data.gov.in/

Enigma: https://public.enigma.com/

Data search: https://datahub.io/search

## Kaggle contest:

## Zalo AI

